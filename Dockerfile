# This file is a template, and might need editing before it works on your project.
FROM node:14.16-alpine

# Uncomment if use of `process.dlopen` is necessary
RUN apk add --no-cache libc6-compat

ARG cnb_uid=1000
ARG cnb_gid=1001

ARG NODE_ENV=production
ENV NODE_ENV $NODE_ENV

# Set required CNB information
ENV CNB_USER_ID=${cnb_uid}
ENV CNB_GROUP_ID=${cnb_gid}

# Install packages that we want to make available at both build and run time
RUN apk add --update --no-cache bash ca-certificates

ENV PORT 5000
EXPOSE 5000

ENV HOME=/home/gatbit
WORKDIR /home/gatbit

RUN npm install --global @teambit/bvm  && bvm install

ENV PATH "${PATH}:${HOME}/bin"
ENTRYPOINT [ "bash", "-C" ]

# USER ${CNB_USER_ID}
# CMD [ "bit", "start" ]

